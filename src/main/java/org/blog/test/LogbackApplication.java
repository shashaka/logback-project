package org.blog.test;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class LogbackApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(LogbackApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.error("error");
        log.warn("warn");
        log.info("info");
        log.debug("debug");
        log.trace("trace");
    }
}